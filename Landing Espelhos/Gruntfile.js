module.exports = function(grunt) {
    var config, environment, errorHandler, name, open, pkg, taskArray, taskName, tasks, verbose, _results;
    pkg = grunt.file.readJSON('package.json');
    environment = process.env.VTEX_HOST || 'vtexcommerce';
    verbose = grunt.option('verbose');
    storename = pkg.accountName;
    pathFiles = pkg.pathFiles;
    open = pkg.accountName ? "http://" + pkg.accountName + ".vtexlocal.com.br/?debugcss=true&debugjs=true" : void 0;
    errorHandler = function(err, req, res, next) {
        var errString, _ref, _ref1;
        errString = (_ref = (_ref1 = err.code) != null ? _ref1.red : void 0) != null ? _ref : err.toString().red;
        return grunt.log.warn(errString, req.url.yellow);
    };
    config = {
        clean: {
            main: ['build']
        },
        compass: {
            dist: {
                options: {
                    sassDir: 'src/assets/scss/',
                    cssDir: 'build/assets/css/',
                    imagesDir: 'src/assets/img/',
                    environment: 'production',
                    //require: ['breakpoint'],
                    sourcemap: false
                }
            }
        },

        uglify: {
            options: {
                mangle: false
            },
            main: {
                files: [{
                    expand: true,
                    cwd: 'src/assets/js/',
                    src: ['**/*.js', ],
                    dest: 'build/assets/js/',
                    ext: '.js'
                }]
            }
        },
        imagemin: {
            main: {
                files: [{
                    expand: true,
                    cwd: 'src/assets/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'build/assets/img'
                }]
            }
        },
        sprite: {
            all: {
                src: 'src/assets/img/sprite/*.png',
                destImg: 'build/assets/img/x-sprite-taco.png',
                destCSS: 'src/assets/scss/_x-sprite.scss',
                cssFormat: 'scss',
                spriteName: 'all',
                padding: 2,
                imgPath: (function() {
                    return ''+ pathFiles +'/x-sprite-taco.png?' + Math.random();
                }())
            },
            mobile: {
                src: 'src/assets/img/sprite_mobile/*.png',
                destImg: 'build/assets/img/x-sprite_mobile-taco.png',
                destCSS: 'src/assets/scss/_x-sprite-mobile.scss',
                cssFormat: 'scss',
                spriteName: 'mobile',
                padding: 2,
                imgPath: (function() {
                   return ''+ pathFiles + '/x-sprite_mobile-taco.png?' + Math.random();
               }())
            }
        },
        webfont: {
            icons: {
                src: 'src/assets/img/icons/*.svg',
                dest: 'build/assets/fonts',
                destCss: 'src/assets/scss',
                options: {
                    syntax: 'bootstrap',
                    font: 'x-icon',
                    engine: 'node',
                    stylesheet: 'scss',
                    relativeFontPath: '../fonts',
                    templateOptions: {
                        classPrefix: 'x-icon-',
                        mixinPrefix: 'x-icon-'
                    }
                }
            }
        },
        copy: {
            main: {
                files: [
                    // makes all src relative to cwd
                    {
                        expand: true,
                        cwd: 'src/cache/',
                        src: ['**'],
                        dest: 'build/assets/cache/'
                    }
                    ]
                }
            },
            
            watch: {
                options: {
                    livereload: 1337
                },
                compass: {
                    files: ['src/assets/scss/**/*.scss'],
                    tasks: ['compass']
                },
                images: {
                    files: ['src/assets/img/**/*.{png,jpg,gif}'],
                    tasks: ['sprite']
                },
                css: {
                    files: ['build/css/**/*.css']
                },
                js: {
                    files: ["src/assets/js/**/*.js"],
                    tasks: ["uglify"]
                }
            }
    };

    tasks = {
        build: ['clean', 'uglify', 'sprite', 'compass', 'copy'],
        "default": ['build', 'watch']
    };
    grunt.initConfig(config);
    for (name in pkg.devDependencies) {
        if (name.slice(0, 6) === 'grunt-') {
            grunt.loadNpmTasks(name);
        }
    }
    _results = [];
    for (taskName in tasks) {
        taskArray = tasks[taskName];
        _results.push(grunt.registerTask(taskName, taskArray));
    }
    return _results;
};
