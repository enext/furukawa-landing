
import Taskerify from 'taskerify';

Taskerify.config.sourcemaps    = false;
Taskerify.config.srcPath       = './assets';  // Src Path
Taskerify.config.distPath      = './dist/assets'; // Dist Path

const SRC          = Taskerify.config.srcPath;
const DIST         = Taskerify.config.distPath;

const storeName    = 'furukawa';
const commomFiles  = ['smartgrid', 'geracao'];

Taskerify((mix) => {
    // Image Minifier
    mix.imagemin(`${SRC}/images`, `${DIST}/images`);

    // Desktop Files
    commomFiles.map((file) => {
        mix.browserify(`${SRC}/js/${storeName}-common-${file}.js`, `${DIST}/js`)
            .sass(`${SRC}/scss/${storeName}-common-${file}.scss`,  `${DIST}/css`);
    });
});
