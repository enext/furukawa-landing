const Methods = {
    init: () => {
        Methods.swipe();
    },

    swipe: () => {
        const swiper = new Swiper(`.swiper-container`, {
            breakpoints: {
                320: {
                    loop: true,
                },      
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', Methods.init);
