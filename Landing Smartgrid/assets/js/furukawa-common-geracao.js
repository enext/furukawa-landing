const Methods = {
    init: () =>{
        Methods.swipe();
    },

    swipe: () => {
        const swiper = new Swiper(`.swiper-container`, {
            autoHeight: true, //enable auto height
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
        });
    },
}

document.addEventListener('DOMContentLoaded', Methods.init);
